#include <iostream>
#include <string>

using namespace std;

int main(void) {

	string name;
	string file_name;
	int strlen;

	cout << "What's your name? ";
	cin >> name;
	
	cout << "What's the name of the output file? ";
	cin >> file_name;
    

    strlen = file_name.length();

	// A loop to Iterate through string to find "."

    bool check = false;

    for (int i = 0; i < strlen + 1 ; i++) {
    	
    // if loop finds the . then we put check = true and add it to the end of the string
    	if (file_name[i] == '.') {
    		check = true;
    	}
    }
    if (check=true){
    		file_name = file_name + ".cpp";
    	}
    	
	cout << "\n"
		 << "// " << file_name
		 << "\n\n";

	cout << "#include <iostream>\n\n"
		 << "int main() {\n"
		 << "    std::cout << \"Hello "
		 << name 
		 << "!"
		 << "\\n\";"
		 << "\n"
		 << "}\n";
}
